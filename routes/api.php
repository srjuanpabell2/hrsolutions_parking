<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::resource('vehicles', 'VehicleAPIController');

Route::resource('vehicle_types', 'VehicleTypeAPIController');

Route::resource('rates', 'RateAPIController');

Route::resource('historicals', 'HistoricalAPIController');

Route::resource('vehicle_types', 'VehicleTypeAPIController');