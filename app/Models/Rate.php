<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Rate
 * @package App\Models
 * @version January 14, 2018, 5:15 pm UTC
 *
 * @property unsignedInteger vehicle_type
 * @property double minute_value
 */
class Rate extends Model
{
    use SoftDeletes;

    public $table = 'rates';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'vehicle_type_id',
        'minute_value'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'minute_value' => 'double'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'vehicle_type_id' => 'required',
        'minute_value' => 'required'
    ];

    public function vehicle_type()
    {
        return $this->belongsTo(VehicleType::class);
    }
    
}
