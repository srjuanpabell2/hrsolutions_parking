<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Vehicle
 * @package App\Models
 * @version January 14, 2018, 5:10 pm UTC
 *
 * @property string identification
 * @property unsignedInteger vehicle_type
 */
class Vehicle extends Model
{
    use SoftDeletes;

    public $table = 'vehicles';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'identification',
        'vehicle_type_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'identification' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'identification' => 'required',
        'vehicle_type_id' => 'required'
    ];

    public function vehicle_type()
    {
        return $this->belongsTo(VehicleType::class);
    }

    
}
