<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class VehicleType
 * @package App\Models
 * @version January 14, 2018, 6:20 pm UTC
 *
 * @property string name
 */
class VehicleType extends Model
{
    use SoftDeletes;

    public $table = 'vehicle_types';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'name'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required'
    ];

    public function rate()
    {
        return $this->hasOne(Rate::class);
    }
    
}
