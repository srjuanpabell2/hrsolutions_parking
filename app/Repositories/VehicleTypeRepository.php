<?php

namespace App\Repositories;

use App\Models\VehicleType;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class VehicleTypeRepository
 * @package App\Repositories
 * @version January 14, 2018, 6:20 pm UTC
 *
 * @method VehicleType findWithoutFail($id, $columns = ['*'])
 * @method VehicleType find($id, $columns = ['*'])
 * @method VehicleType first($columns = ['*'])
*/
class VehicleTypeRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return VehicleType::class;
    }
}
