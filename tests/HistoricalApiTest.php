<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class HistoricalApiTest extends TestCase
{
    use MakeHistoricalTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateHistorical()
    {
        $historical = $this->fakeHistoricalData();
        $this->json('POST', '/api/v1/historicals', $historical);

        $this->assertApiResponse($historical);
    }

    /**
     * @test
     */
    public function testReadHistorical()
    {
        $historical = $this->makeHistorical();
        $this->json('GET', '/api/v1/historicals/'.$historical->id);

        $this->assertApiResponse($historical->toArray());
    }

    /**
     * @test
     */
    public function testUpdateHistorical()
    {
        $historical = $this->makeHistorical();
        $editedHistorical = $this->fakeHistoricalData();

        $this->json('PUT', '/api/v1/historicals/'.$historical->id, $editedHistorical);

        $this->assertApiResponse($editedHistorical);
    }

    /**
     * @test
     */
    public function testDeleteHistorical()
    {
        $historical = $this->makeHistorical();
        $this->json('DELETE', '/api/v1/historicals/'.$historical->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/historicals/'.$historical->id);

        $this->assertResponseStatus(404);
    }
}
