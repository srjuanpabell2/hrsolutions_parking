<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class VehicleTypeApiTest extends TestCase
{
    use MakeVehicleTypeTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateVehicleType()
    {
        $vehicleType = $this->fakeVehicleTypeData();
        $this->json('POST', '/api/v1/vehicleTypes', $vehicleType);

        $this->assertApiResponse($vehicleType);
    }

    /**
     * @test
     */
    public function testReadVehicleType()
    {
        $vehicleType = $this->makeVehicleType();
        $this->json('GET', '/api/v1/vehicleTypes/'.$vehicleType->id);

        $this->assertApiResponse($vehicleType->toArray());
    }

    /**
     * @test
     */
    public function testUpdateVehicleType()
    {
        $vehicleType = $this->makeVehicleType();
        $editedVehicleType = $this->fakeVehicleTypeData();

        $this->json('PUT', '/api/v1/vehicleTypes/'.$vehicleType->id, $editedVehicleType);

        $this->assertApiResponse($editedVehicleType);
    }

    /**
     * @test
     */
    public function testDeleteVehicleType()
    {
        $vehicleType = $this->makeVehicleType();
        $this->json('DELETE', '/api/v1/vehicleTypes/'.$vehicleType->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/vehicleTypes/'.$vehicleType->id);

        $this->assertResponseStatus(404);
    }
}
