<?php

use App\Models\VehicleType;
use App\Repositories\VehicleTypeRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class VehicleTypeRepositoryTest extends TestCase
{
    use MakeVehicleTypeTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var VehicleTypeRepository
     */
    protected $vehicleTypeRepo;

    public function setUp()
    {
        parent::setUp();
        $this->vehicleTypeRepo = App::make(VehicleTypeRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateVehicleType()
    {
        $vehicleType = $this->fakeVehicleTypeData();
        $createdVehicleType = $this->vehicleTypeRepo->create($vehicleType);
        $createdVehicleType = $createdVehicleType->toArray();
        $this->assertArrayHasKey('id', $createdVehicleType);
        $this->assertNotNull($createdVehicleType['id'], 'Created VehicleType must have id specified');
        $this->assertNotNull(VehicleType::find($createdVehicleType['id']), 'VehicleType with given id must be in DB');
        $this->assertModelData($vehicleType, $createdVehicleType);
    }

    /**
     * @test read
     */
    public function testReadVehicleType()
    {
        $vehicleType = $this->makeVehicleType();
        $dbVehicleType = $this->vehicleTypeRepo->find($vehicleType->id);
        $dbVehicleType = $dbVehicleType->toArray();
        $this->assertModelData($vehicleType->toArray(), $dbVehicleType);
    }

    /**
     * @test update
     */
    public function testUpdateVehicleType()
    {
        $vehicleType = $this->makeVehicleType();
        $fakeVehicleType = $this->fakeVehicleTypeData();
        $updatedVehicleType = $this->vehicleTypeRepo->update($fakeVehicleType, $vehicleType->id);
        $this->assertModelData($fakeVehicleType, $updatedVehicleType->toArray());
        $dbVehicleType = $this->vehicleTypeRepo->find($vehicleType->id);
        $this->assertModelData($fakeVehicleType, $dbVehicleType->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteVehicleType()
    {
        $vehicleType = $this->makeVehicleType();
        $resp = $this->vehicleTypeRepo->delete($vehicleType->id);
        $this->assertTrue($resp);
        $this->assertNull(VehicleType::find($vehicleType->id), 'VehicleType should not exist in DB');
    }
}
