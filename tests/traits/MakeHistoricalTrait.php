<?php

use Faker\Factory as Faker;
use App\Models\Historical;
use App\Repositories\HistoricalRepository;

trait MakeHistoricalTrait
{
    /**
     * Create fake instance of Historical and save it in database
     *
     * @param array $historicalFields
     * @return Historical
     */
    public function makeHistorical($historicalFields = [])
    {
        /** @var HistoricalRepository $historicalRepo */
        $historicalRepo = App::make(HistoricalRepository::class);
        $theme = $this->fakeHistoricalData($historicalFields);
        return $historicalRepo->create($theme);
    }

    /**
     * Get fake instance of Historical
     *
     * @param array $historicalFields
     * @return Historical
     */
    public function fakeHistorical($historicalFields = [])
    {
        return new Historical($this->fakeHistoricalData($historicalFields));
    }

    /**
     * Get fake data of Historical
     *
     * @param array $postFields
     * @return array
     */
    public function fakeHistoricalData($historicalFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'comments' => $fake->word,
            'vehicle_id' => $fake->word,
            'start_time' => $fake->word,
            'end_time' => $fake->word,
            'total_value' => $fake->word,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $historicalFields);
    }
}
